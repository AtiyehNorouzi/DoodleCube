﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour , IStatic {
    public enum ObstacleType { TIGH , BOX }
    [SerializeField]
    private ObstacleType type;
    public void init()
    {
        StartCoroutine(CheckDeadZone());
    }
  
    void OnCollisionEnter2D(Collision2D col)
    {
        if (type == ObstacleType.TIGH)
        {
            if (col.gameObject.tag == "cube")
                GameState.instance.Pause();
            if (col.gameObject.tag == "bullet")
                Despawn();
        }
    }
    public void Despawn()
    {
        if (type == ObstacleType.BOX)
            ObjectPooler.instance.pushBox(this);
        else
            ObjectPooler.instance.pushTigh(this);

    }
    public IEnumerator CheckDeadZone()
    {
        while (true)
        {
            if(transform.position.y <= GameManager.instance.DeadZonePos.position.y)
                Despawn();
            yield return new WaitForEndOfFrame();
        }
    }
}
