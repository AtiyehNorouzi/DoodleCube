﻿ using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour  , IMovable{
    public enum BulletType {CharacterBullet , EnemyBullet}
    bool moving ;
    public BulletType type;
    Vector3 dir;
    
    public void init(Vector3 dir , float speed)
    {
        moving = true;
        StartCoroutine(Move(dir, speed));
    }
    

    public IEnumerator Move(Vector3 dir, float speed)
    {
        while (moving) {
            transform.Translate(dir * speed * TimeManager.deltaTime);
            yield return new WaitForEndOfFrame();
        }
       
    }
    void OnTriggerEnter2D(Collider2D col)
    {


        if (col.gameObject.tag == "cube")
            GameState.instance.Pause();
        
     
    }
 
    public void Despawn()
    {

        moving = false;
        if (type == BulletType.CharacterBullet)
            ObjectPooler.instance.pushBullet(this);
        else
            ObjectPooler.instance.pushEBullet(this);
    }

    public IEnumerator CheckDeadZone()
    {
        while (moving)
        {
            if (transform.position.y < GameManager.instance.DeadZonePos.position.y || transform.position.x <= GameManager.instance.LeftZonePos.position.x || transform.position.x >= GameManager.instance.RightZonePos.position.x)
            {
                moving = false;
                Despawn();
            }
            yield return new WaitForEndOfFrame();
        }
    }
}
