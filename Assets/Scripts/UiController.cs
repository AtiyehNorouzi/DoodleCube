﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class UiController : MonoBehaviour {
    public GameObject panel;
    public Button retryBtn;
    public Button quitBtn;
    public Text text;
    private static UiController _instance;
    public static UiController instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<UiController>();
            return _instance;
        }
    }

	// Use this for initialization
	void Start () {
        retryBtn.onClick.AddListener(delegate () { Retry(); });
        quitBtn.onClick.AddListener(delegate () { Quit(); });
        StartCoroutine(timeCounter());
    }
    public void ActiveRetryPanel()
    {
        panel.SetActive(true);
    }
    void Retry()
    {
       
        GameState.gamestate = GameState.gameState.before;
        SceneManager.LoadScene("DoodleCube");
    }
    IEnumerator timeCounter()
    {
        while(true)
        {
            if (GameState.gamestate == GameState.gameState.isStarted)
            {
                text.text = Time.time.ToString("0.00");
            }
            yield return null;
        }

    }
    void Quit()
    {
        Application.Quit();
    }
}
