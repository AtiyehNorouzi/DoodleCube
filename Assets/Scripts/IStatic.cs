﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStatic {
    IEnumerator CheckDeadZone();
    void Despawn();
}
