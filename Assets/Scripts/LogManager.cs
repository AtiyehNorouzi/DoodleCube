﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogManager : MonoBehaviour
{
    [SerializeField]
    private Text[] _text;

    //Public Methods
    public void LogIt(string textLog, int index)
    {
        _text[index].text = textLog;
    }

    //Instance
    private static LogManager _instance;
    public static LogManager instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<LogManager>();

            return _instance;
        }
    }
}
