﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility : MonoBehaviour
{

    public static bool almostEqual(float a, float b, float dist)
    {
        if (Mathf.Abs(a - b) >= dist)
            return false;
        return true;
    }
    public static bool almostEqual(Vector3 a, Vector3 b, float dist)
    {
        if (Mathf.Abs(a.x - b.x) >= dist) return false;
        else if (Mathf.Abs(a.y - b.y) >= dist) return false;
        else if (Mathf.Abs(a.z - b.z) >= dist) return false;
        else return true;
    }
}
