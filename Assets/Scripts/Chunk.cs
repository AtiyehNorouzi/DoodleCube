﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chunk : MonoBehaviour {
    bool isEnabled = false;
    public chunkPooler.chunkType type;
    private float offset = 5f;
	void OnEnable()
    {
        isEnabled = true;
        StartCoroutine(CheckDeadZone());
    }
    public void Despawn()
    {
        chunkPooler.instance.deactive(gameObject ,(int) type);
    }
    public IEnumerator CheckDeadZone()
    {
        while (isEnabled)
        {

            if (transform.position.y + offset <= GameManager.instance.DeadZonePos.position.y)
            {
                isEnabled = false;
                Despawn();
            }
            yield return new WaitForEndOfFrame();
        }

    }
}
