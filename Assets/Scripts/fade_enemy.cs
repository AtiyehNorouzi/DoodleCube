﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fade_enemy : MonoBehaviour, IStatic {
    bool isEnabled = true;
    public void init()
    {
        isEnabled = true;
        StartCoroutine(CheckDeadZone());
    }

    public IEnumerator CheckDeadZone()
    {
        while (isEnabled)
        {

            if (transform.position.y <= GameManager.instance.DeadZonePos.position.y)
            {
              
                isEnabled = false;
                Despawn();

            }
            yield return new WaitForEndOfFrame();
        }

    }

    public void Despawn()
    {
        ObjectPooler.instance.pushFade(this);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "cube")
            GameState.instance.Pause();
        else if (col.gameObject.tag == "bullet")
            Despawn();
    }
  

}
