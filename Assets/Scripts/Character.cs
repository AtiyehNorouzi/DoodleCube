﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*character moving script
 * character's states:jump , bounce , move , shoot
 */
public class Character : MonoBehaviour, IMovable
{
    private Rigidbody2D rigidbody;
    bool moving = false;
    private float deadZone;
    private float rightZone;
    private float leftZone;
    private const float horizontalSpeed = 250f;
    private const float JumpSpeed = 350f;
    private const float JumpOffset = 1f;
    public enum onAirState { FALL, JUMP , BOUNCE }
    public onAirState state = onAirState.FALL;
    void Start()
    {
        deadZone = GameManager.instance.DeadZonePos.position.y;
        rightZone = GameManager.instance.RightZonePos.position.x;
        leftZone = GameManager.instance.LeftZonePos.position.x;
        rigidbody = GetComponent<Rigidbody2D>();
        StartCoroutine(Exit_Enter());
        StartCoroutine(CheckDeadZone());
        InputController.instance._OnPressed += InputListener;
        InputController.instance._GetAxis += InputListener;        
    }

    public IEnumerator Move(Vector3 dir, float speed)
    {
        if (moving)
            rigidbody.AddForce(dir * speed);
        yield return null;
    }
    // Update is called once per frame
    void InputListener(InputController.InputType type)
    {
        switch (type)
        {
            case InputController.InputType.Jump:
                {
                    Jump();
                    break;
                }
       
            case InputController.InputType.Shoot:
                {
                    
                    shoot();
                    break;
                }
        }

    }
    void InputListener(InputController.InputType type , float value)

    {
       
        switch (type)
        {
            case InputController.InputType.LeftMove:
                {
                    
                    HorizontalMove(value);
                    break;
                }
            case InputController.InputType.RightMove:
                {
                    HorizontalMove(value);
                    break;
                }
        }
    }

  
    void Update()
    {
      
        #region fall
        if (rigidbody.velocity.y <= 0 && state == onAirState.JUMP)
            fall();

        #endregion
    }
    void HorizontalMove(float value)
    {
        if(value > 0)
            CharacterAnimation.instance.startAnimation(CharacterAnimation.animationStates.move_right);
        else
            CharacterAnimation.instance.startAnimation(CharacterAnimation.animationStates.move_left);
        float x = value * TimeManager.deltaTime * horizontalSpeed;
        rigidbody.velocity = new Vector2(x, rigidbody.velocity.y);
    }
    void fall()
    {
        state = onAirState.FALL;
        CharacterAnimation.instance.startAnimation(CharacterAnimation.animationStates.fall);
    }
    void Jump()
    {
     
        if ( state == onAirState.FALL && Utility.almostEqual(rigidbody.velocity.y, 0, JumpOffset) )
        {
            moving = true;
            CharacterAnimation.instance.startAnimation(CharacterAnimation.animationStates.jump);
            Invoke("ChangeJumpState", 0.1f);
            StartCoroutine(Move(Vector3.up, JumpSpeed));
            Invoke("StopRoutine", 0.09f);

        }
    }
    void shoot()
    {
        Bullet bullet = ObjectPooler.instance.GetBullet();
        bullet.gameObject.transform.position = transform.position;
        #region set bullet direction
        Vector3 mousePositionInWorldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 dir = (mousePositionInWorldPoint - transform.position).normalized;
        bullet.init(dir, 15f);
      
        #endregion
    }

    void StopRoutine()
    {
        moving = false;
    }
   
    IEnumerator  Exit_Enter()
    {
        while (true)
        {
            if (transform.position.x < leftZone)
                transform.position = new Vector3(rightZone, transform.position.y, transform.position.z);
            else if(transform.position.x >= rightZone)
                transform.position = new Vector3(leftZone, transform.position.y, transform.position.z);
            yield return new WaitForEndOfFrame();
        }
    }
    public void Despawn()
    {
        throw new NotImplementedException();
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "enemyBullet")
            GameState.instance.Pause();

        if (col.gameObject.tag == "tile" )
            if(col.gameObject.GetComponent<Tile>().currentState == Tile.tileState.BOUNCE)
                rigidbody.velocity = Vector3.Reflect(new Vector3(col.relativeVelocity.x, -10, 0), Vector3.up);

    }
    public void ChangeJumpState()
    {
        state = (state == onAirState.FALL) ? onAirState.JUMP : onAirState.FALL;
    }
    public IEnumerator CheckDeadZone()
    {
        while (true)
        {
            if(transform.position.y <= deadZone)
            {
                GameState.instance.Pause();
            }
            yield return new WaitForEndOfFrame();
        }
    }
}
   

