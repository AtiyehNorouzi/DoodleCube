﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController :   MonoBehaviour
{
    //fields
    private static AnimationController _instance;
    bool animationPLaying = true;

    //property
    public static AnimationController instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<AnimationController>();
            return _instance;
        }
    }


    public void startAnimation(Animator animator , string name , bool backToIdle)
    {
        animator.Play(name);
        animationPLaying = true;
        if(backToIdle)
              StartCoroutine(stopAnimation(animator, name));
    }
    public bool AnimatorIsPlaying(Animator animator)
    {
    
        return animator.GetCurrentAnimatorStateInfo(0).length >
               animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }
    bool AnimatorIsPlaying(Animator animator , string stateName)
    {
        return AnimatorIsPlaying(animator) && animator.GetCurrentAnimatorStateInfo(0).IsName(stateName);
    }
    public IEnumerator stopAnimation(Animator animator ,  string animationName)
    {
        while (animationPLaying)
        {
  
            if (!AnimatorIsPlaying(animator , name))
            {
                animator.SetBool("move", false);
                animationPLaying = false;
            }
            yield return null;
        }
    }

}
