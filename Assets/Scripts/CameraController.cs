﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	 #region  sources
    public Transform target;
    float smoothspeed = 0.125f;
    float offset;
    float velocity = 0;
    public static bool OnTarget = false;
    #endregion
    void Start()
    {
        //the distance camera has from the gole position 
        offset = 2;
    }
	void LateUpdate () {

        if (OnTarget)
        {  
            float destpos = target.position.y + offset;
            transform.position = new Vector3(transform.position.x, Mathf.SmoothDamp(transform.position.y, destpos, ref velocity, 0.3f) , transform.position.z);
            GameManager.instance.camera.orthographicSize = Mathf.SmoothDamp(GameManager.instance.camera.orthographicSize, 5.5f, ref velocity, 10f);
            if (GameManager.instance.camera.orthographicSize <= 2.5f)
            {
                
                OnTarget = false;
            }
         } 
    }

}
