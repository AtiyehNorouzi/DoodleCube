﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour {
    public enum level { EASY , NORMAL , HARD , VERYHARD , MASTER}
    [System.Serializable]
    public struct levelStruct
    {
        public level levelType;
        public int waveNumbers;
    }
    [Header("Levels")]
    public levelStruct [] levels;
    public int currentLevel = 0 ;
    float levelCounter = 0 ;
    private GameObject ChunkComing;
    private GameObject ChunkLeaving;
    private GameObject ThirdChunkComing;
    private Rigidbody2D chunkComingRB2;
    private Rigidbody2D chunkLeavingRB2;
    private Rigidbody2D chunkThirdRB2;
    private const float speed = 50f;
    [Header("Scene Positions")]
    public Transform startPosition;
    public Transform middlePosition;
    public Transform endPosition;
    public Transform ThirdPosition;
    Vector3 dir;
    private bool firstTile = true;
    void Start ()
    {
        dir = (middlePosition.position - startPosition.position).normalized;     
        InitializeChunks();
        Invoke("waitToPressKey", 0);
	}
	void waitToPressKey()
    {

        if (GameState.gamestate == GameState.gameState.isStarted)
        {
            StartCoroutine(levelChanger());
            StartCoroutine(sceneChanger());
            StartCoroutine(moveChunks());
        }
        else
            Invoke("waitToPressKey", 1f);
    }
	void InitializeChunks()
    {
        ChunkInitializer(ref ChunkLeaving , middlePosition.transform , ref chunkLeavingRB2);
        ChunkInitializer(ref ChunkComing , startPosition.transform , ref chunkComingRB2);
        ChunkInitializer(ref ThirdChunkComing, ThirdPosition.transform, ref chunkThirdRB2);
    }
    public void ChunkInitializer(ref GameObject obj , Transform transform , ref Rigidbody2D rb)
    {

        obj = chunkPooler.instance.GetPooledObject(getObjectType(levels[currentLevel].levelType));
        obj.transform.position = transform.position;
        obj.transform.rotation = transform.rotation;
        rb = obj.GetComponent<Rigidbody2D>();
        SpawnObjs(obj);
        levelCounter++;
    }
    public IEnumerator levelChanger()
    {
        while (true)
        {
            if (levelCounter >= levels[currentLevel].waveNumbers )
            {
                levelCounter = 0;
                currentLevel++;
            }
            yield return new WaitForEndOfFrame();
        }
    }
    public IEnumerator sceneChanger()
    {
        while (true)
        {

            if (Vector3.Distance(ChunkComing.transform.position, endPosition.transform.position) <= Vector3.Distance(ChunkComing.transform.position, startPosition.transform.position))
            {

                ChunkLeaving = ChunkComing;
                ChunkComing = ThirdChunkComing;
                //ChunkLeaving.transform.position = middlePosition.position;
                //ChunkLeaving.transform.rotation = middlePosition.rotation;                                    
                ChunkInitializer(ref ThirdChunkComing, ThirdPosition.transform, ref chunkThirdRB2);
            }
            yield return new WaitForEndOfFrame();
        }
    }
    public void SpawnObjs(GameObject obj)
    {
        #region setTiles
        setTiles(obj.transform.GetChild(0).gameObject);
        #endregion
        #region spawnEnemies
        setBoxEnemy(obj);
        setFadeEnemy(obj);
        setTighEnemy(obj);
        setShootEnemy(obj);
        setMoveEnemy(obj);
       
        #endregion
    }
    void setTiles(GameObject obj)
    {
        //iterate on tile types
        for(int i=0;i< obj.transform.childCount; i++)
        {
            for (int j = 0; j < obj.transform.GetChild(i).childCount; j++)
            {
                Tile tileObj = ObjectPooler.instance.GetTile();
                tileObj.transform.SetParent(obj.transform.GetChild(i).transform.GetChild(j));
                tileObj.transform.localPosition = Vector3.zero;
                tileObj.transform.localRotation = Quaternion.identity;
                //static
                if (i == 0)
                {
                    if (firstTile)
                        firstTile = false;
                    else
                        tileObj.init(setTileState(obj.transform.GetChild(i).gameObject));
                }
                else
                {
                    switch (i)
                    {
                        //bounce
                        case 1:
                            {
                                tileObj.init(Tile.tileState.BOUNCE);
                                break;
                            }
                            //move vertical
                        case 2:
                            {
                                tileObj.init(Tile.tileState.MOVEUP);
                                break;
                            
                            }
                            //move horizontal
                        case 3:
                            {
                                tileObj.init(Tile.tileState.MOVE);
                                break;
                            }
                    }

                }
                }

        }
    }

    Tile.tileState setTileState(GameObject obj)
    {

        int rand;
     
            switch (currentLevel)
            {

                case 0:
                    {
                        rand = getRandom(0 , 4);
                        if (rand < 1 && !firstTile)
                            return Tile.tileState.ROTATE;
                        return Tile.tileState.STATIC;
                    }

                case 1:
                    {
                        rand = getRandom(0 , 5);
                        if (rand < 1)
                            return Tile.tileState.MOVE;
                        else if (rand < 3)
                            return Tile.tileState.BREAK;
                        return Tile.tileState.STATIC;
                    }

                default:
                    {
                        rand = getRandom(0 , 8);
                        if (rand < 2)
                            return Tile.tileState.MOVE;
                        else if (rand < 3)
                            return Tile.tileState.BREAK;
                        else if (rand < 4)
                            return Tile.tileState.ROTATE;
                        return Tile.tileState.STATIC;

                    }
            
        }
    }

    #region SetEnemies
    public void setBoxEnemy(GameObject obj)
    {
        int childLength = obj.transform.Find("Box_parents").childCount;
        for(int i =0;i < childLength;i++)
        {
            Obstacle BoxGO = ObjectPooler.instance.GetBox();
            BoxGO.transform.SetParent(obj.transform.Find("Box_parents").transform.GetChild(i).transform);
            BoxGO.transform.localPosition = Vector3.zero;
            BoxGO.transform.localRotation = Quaternion.identity;
            BoxGO.init();
        }


    }
    public void setFadeEnemy(GameObject obj)
    {
   
        int childLength = obj.transform.Find("fade_parents").childCount;
        for (int i = 0; i < childLength; i++)
        {
            fade_enemy FadeGO = ObjectPooler.instance.GetFade();
            FadeGO.transform.SetParent(obj.transform.Find("fade_parents").transform.GetChild(i).transform);
            FadeGO.transform.localPosition = Vector3.zero;
            FadeGO.transform.localRotation = Quaternion.identity;
            FadeGO.init();
        }
        

    }
    public void setTighEnemy(GameObject obj)
    {
        int childLength = obj.transform.Find("Tigh_parents").childCount;
        for (int i = 0; i < childLength; i++)
        {
            Obstacle TighGO = ObjectPooler.instance.GetTigh();
                TighGO.transform.SetParent(obj.transform.Find("Tigh_parents").transform.GetChild(i).transform);
                TighGO.transform.localPosition = Vector3.zero;
                TighGO.transform.localRotation = Quaternion.identity;
                TighGO.init();
            }
        

    }
    public void setMoveEnemy(GameObject obj)
    {
        int childLength = obj.transform.Find("move_parents").childCount;
        for (int i = 0; i < childLength; i++)
        {
                move_enemy moveGO = ObjectPooler.instance.GetMove();
                moveGO.transform.SetParent(obj.transform.Find("move_parents").transform.GetChild(i).transform);
                moveGO.transform.localPosition = Vector3.zero;
                moveGO.transform.localRotation = Quaternion.identity;
                moveGO.init();
            
        }


    }
    public void setShootEnemy(GameObject obj)
    {
        int childLength = obj.transform.Find("fire_parents").childCount;
        for (int i = 0; i < childLength; i++)
        {
                fire_enemy fireGO = ObjectPooler.instance.GetFire();
                fireGO.transform.SetParent(obj.transform.Find("fire_parents").transform.GetChild(i).transform);
                fireGO.transform.localPosition = Vector3.zero;
                fireGO.transform.localRotation = Quaternion.identity;
                fireGO.init();
            
        }

    }
    #endregion

    int getRandom(int from , int to)
    {
        return Random.Range(from, to);
    }
 
  
    public IEnumerator moveChunks()
    {
        while (true)
        {

            chunkComingRB2.velocity = dir * TimeManager.deltaTime * speed;
            chunkLeavingRB2.velocity = dir * TimeManager.deltaTime * speed;
            chunkThirdRB2.velocity = dir * TimeManager.deltaTime * speed;
            yield return new WaitForEndOfFrame();
        }
      
    }
    chunkPooler.chunkType getObjectType(level type)
    {
        int random;
        random = getRandom(0, 20);
        switch (type)
            {
            case level.EASY:
                {
                   
                    if(random > 8 )
                        return chunkPooler.chunkType.easyChunk;
                    else 
                        return chunkPooler.chunkType.normalChunk;
                   
                }
            case level.NORMAL:
                {
                    if (random > 8)
                        return chunkPooler.chunkType.normalChunk;
                    else if (random <= 3)
                        return chunkPooler.chunkType.hardChunk;
                    else
                        return chunkPooler.chunkType.easyChunk;
                }
            case level.HARD:
                {
                    if (random > 5)
                        return chunkPooler.chunkType.hardChunk;
                    else if (random <= 3)
                        return chunkPooler.chunkType.easyChunk;
                    else
                        return chunkPooler.chunkType.normalChunk;
                }
            case level.VERYHARD:
                {
                    return chunkPooler.chunkType.hardChunk;
                }
            case level.MASTER:
                {
                    return chunkPooler.chunkType.masterChunk;
                }


        }
        return chunkPooler.chunkType.easyChunk;

    }



}
