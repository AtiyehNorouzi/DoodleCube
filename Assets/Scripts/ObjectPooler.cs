﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour {
    List<Component> components;
 
    #region poolObjectsStruct
    [System.Serializable]
    public struct BulletObjStruct
    {
 
        public int amount;
        public GameObject prefab;
        public Stack<Bullet> pooledObjs;
        public int amountNow;
    }
    [System.Serializable]
    public struct EnemyBulletObjStruct
    {
        
        public int amount;
        public GameObject prefab;
        public Stack<Bullet> pooledObjs;
        public int amountNow;
    }
    [System.Serializable]
    public struct TileObjStruct
    {
      
        public int amount;
        public GameObject prefab;
        public Stack<Tile> pooledObjs;
        public int amountNow;
    }

    [System.Serializable]
    public struct FadeEnemyObjStruct
    {
   
        public int amount;
        public GameObject prefab;
        public Stack<fade_enemy> pooledObjs;
        public int amountNow;
    }
    [System.Serializable]
    public struct FireEnemyObjStruct
    {
    
        public int amount;
        public GameObject prefab;
        public Stack<fire_enemy> pooledObjs;
        public int amountNow;
    }
    [System.Serializable]
    public struct MoveEnemyObjStruct
    {
       
        public int amount;
        public GameObject prefab;
        public Stack<move_enemy> pooledObjs;
        public int amountNow;
    }
    [System.Serializable]
    public struct ObstacleObjStruct
    {
       
        public int amount;
        public GameObject prefab;
        public Stack<Obstacle> pooledObjs;
        public int amountNow;
    }
 
    #endregion

    public BulletObjStruct bulletsWeWantToPool;
    public TileObjStruct TileWeWantToPool;
    public BulletObjStruct EnemyBulletWeWantToPool;
    public FadeEnemyObjStruct fadeEnemyWeWantToPool;
    public FireEnemyObjStruct fireWeWantToPool;
    public MoveEnemyObjStruct moveWeWantToPool;
    public ObstacleObjStruct tighWeWantToPool;
    public ObstacleObjStruct boxWeWantToPool;
    #region GameObjectStructs parent
    GameObject bulletParent;
    GameObject tileParent;
    GameObject tighParent;
    GameObject moveParent;
    GameObject fadeParent;
    GameObject fireParent;
    GameObject boxParent;
    GameObject ebulletParent;

    #endregion

    // Use this for initialization
    private static ObjectPooler _instance;
    public static ObjectPooler instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<ObjectPooler>();

            return _instance;
        }
    }
    void Awake ()
    {
        _instance = this;
        CreatePools();
	}
    public void CreatePools()
    {
        createBullets();
        createEnemyBullet();
        createfadeEnemy();
        createfireEnemy();
        createmoveEnemy();
        createTiles();
        createBox();
        createTighEnemy();
    
        
    }
   public void createBullets()
    {
        bulletsWeWantToPool.pooledObjs = new Stack<Bullet>();
        bulletsWeWantToPool.amountNow = bulletsWeWantToPool.amount;
        bulletParent = new GameObject(bulletsWeWantToPool.prefab.name + "_parent");
        bulletParent.transform.SetParent(instance.gameObject.transform);
      
        for (int j = 0; j < bulletsWeWantToPool.amount; j++)
        { 
       
            GameObject GO = Instantiate(bulletsWeWantToPool.prefab);
            Bullet c = GO.GetComponent<Bullet>();
            GO.SetActive(false);
            GO.transform.SetParent(bulletParent.transform);
            bulletsWeWantToPool.pooledObjs.Push(c);
        }

    }
    public void createTiles()
    {
        TileWeWantToPool.pooledObjs = new Stack<Tile>();
        TileWeWantToPool.amountNow = TileWeWantToPool.amount;
        tileParent = new GameObject(TileWeWantToPool.prefab.name + "_parent");
        tileParent.transform.SetParent(instance.gameObject.transform);
        for (int j = 0; j < TileWeWantToPool.amount; j++)
        {

            GameObject GO = Instantiate(TileWeWantToPool.prefab);
            Tile c = GO.GetComponent<Tile>();
            GO.SetActive(false);
            GO.transform.SetParent(tileParent.transform);
            TileWeWantToPool.pooledObjs.Push(c);
        }
    }
    public void createEnemyBullet()
    {
        EnemyBulletWeWantToPool.pooledObjs = new Stack<Bullet>();
        EnemyBulletWeWantToPool.amountNow = EnemyBulletWeWantToPool.amount;
        ebulletParent = new GameObject(EnemyBulletWeWantToPool.prefab.name + "_parent");
        ebulletParent.transform.SetParent(instance.gameObject.transform);
        for (int j = 0; j < EnemyBulletWeWantToPool.amount; j++)
        {
            GameObject GO = Instantiate(EnemyBulletWeWantToPool.prefab);
            Bullet c = GO.GetComponent<Bullet>();
            GO.SetActive(false);
            GO.transform.SetParent(ebulletParent.transform);
            EnemyBulletWeWantToPool.pooledObjs.Push(c);
        }
    }
    public void createfadeEnemy()
    {
        fadeEnemyWeWantToPool.pooledObjs = new Stack<fade_enemy>();
        fadeEnemyWeWantToPool.amountNow = fadeEnemyWeWantToPool.amount;
        fadeParent = new GameObject(fadeEnemyWeWantToPool.prefab.name + "_parent");
        fadeParent.transform.SetParent(instance.gameObject.transform);
        for (int j = 0; j < fadeEnemyWeWantToPool.amount; j++)
        {
            GameObject GO = Instantiate(fadeEnemyWeWantToPool.prefab);
            fade_enemy c = GO.GetComponent<fade_enemy>();
            GO.SetActive(false);
            GO.transform.SetParent(fadeParent.transform);
            fadeEnemyWeWantToPool.pooledObjs.Push(c);
        }
    }
    public void createfireEnemy()
    {
        fireWeWantToPool.pooledObjs = new Stack<fire_enemy>();
        fireWeWantToPool.amountNow = fireWeWantToPool.amount;
        fireParent = new GameObject(fireWeWantToPool.prefab.name + "_parent");
        fireParent.transform.SetParent(instance.gameObject.transform);
        for (int j = 0; j < fireWeWantToPool.amount; j++)
        {
            GameObject GO = Instantiate(fireWeWantToPool.prefab);
            fire_enemy c = GO.GetComponent<fire_enemy>();
            GO.SetActive(false);
            GO.transform.SetParent(fireParent.transform);
            fireWeWantToPool.pooledObjs.Push(c);
        }
    }
    public void createmoveEnemy()
    {
        moveWeWantToPool.pooledObjs = new Stack<move_enemy>();
        moveWeWantToPool.amountNow = moveWeWantToPool.amount;
        moveParent = new GameObject(moveWeWantToPool.prefab.name + "_parent");
        moveParent.transform.SetParent(instance.gameObject.transform);
        for (int j = 0; j < moveWeWantToPool.amount; j++)
        {
            GameObject GO = Instantiate(moveWeWantToPool.prefab);
            move_enemy c = GO.GetComponent<move_enemy>();
            GO.SetActive(false);
            GO.transform.SetParent(moveParent.transform);
            moveWeWantToPool.pooledObjs.Push(c);
        }
    }
    public void createTighEnemy()
    {
        tighWeWantToPool.pooledObjs = new Stack<Obstacle>();
        tighWeWantToPool.amountNow = tighWeWantToPool.amount;
        GameObject tighParent = new GameObject(tighWeWantToPool.prefab.name + "_parent");
        tighParent.transform.SetParent(instance.gameObject.transform);
        for (int j = 0; j < tighWeWantToPool.amount; j++)
        {
            GameObject GO = Instantiate(tighWeWantToPool.prefab);
            Obstacle c = GO.GetComponent<Obstacle>();
            GO.SetActive(false);
            GO.transform.SetParent(tighParent.transform);
            tighWeWantToPool.pooledObjs.Push(c);
        }
    }
    public void createBox()
    {
        boxWeWantToPool.pooledObjs = new Stack<Obstacle>();
        boxWeWantToPool.amountNow = boxWeWantToPool.amount;
        boxParent = new GameObject(boxWeWantToPool.prefab.name + "_parent");
        boxParent.transform.SetParent(instance.gameObject.transform);
        for (int j = 0; j < boxWeWantToPool.amount; j++)
        {
            GameObject GO = Instantiate(boxWeWantToPool.prefab);
            Obstacle c = GO.GetComponent<Obstacle>();
            GO.SetActive(false);
            GO.transform.SetParent(boxParent.transform);
            boxWeWantToPool.pooledObjs.Push(c);
        }
    }
    public void pushTile(Tile tile)
    {
        tile.gameObject.SetActive(false);
        TileWeWantToPool.pooledObjs.Push(tile);
        tile.gameObject.transform.SetParent(tileParent.transform);
        tile.gameObject.transform.localPosition = Vector3.zero;
    }
    public void pushBullet(Bullet bullet)
    {
        bullet.gameObject.SetActive(false);
        bulletsWeWantToPool.pooledObjs.Push(bullet);
        bullet.gameObject.transform.SetParent(bulletParent.transform);
        bullet.gameObject.transform.localPosition = Vector3.zero;
    }
    public void pushEBullet(Bullet ebullet)
    {
        ebullet.gameObject.SetActive(false);
        EnemyBulletWeWantToPool.pooledObjs.Push(ebullet);
        ebullet.gameObject.transform.SetParent(ebulletParent.transform);
        ebullet.gameObject.transform.localPosition = Vector3.zero;
    }
    public void pushMove(move_enemy move)
    {
        move.gameObject.SetActive(false);
        moveWeWantToPool.pooledObjs.Push(move);
        move.gameObject.transform.SetParent(moveParent.transform);
        move.gameObject.transform.localPosition = Vector3.zero;
    }
    public void pushFire(fire_enemy fire)
    {
        fire.gameObject.SetActive(false);
        fireWeWantToPool.pooledObjs.Push(fire);
        fire.gameObject.transform.SetParent(fireParent.transform);
        fire.gameObject.transform.localPosition = Vector3.zero;
    }
    public void pushFade(fade_enemy fade)
    {
        fade.gameObject.SetActive(false);
        fadeEnemyWeWantToPool.pooledObjs.Push(fade);
        fade.gameObject.transform.SetParent(fadeParent.transform);
        fade.gameObject.transform.localPosition = Vector3.zero;
    }
    public void pushBox(Obstacle box)
    {
        box.gameObject.SetActive(false);
        boxWeWantToPool.pooledObjs.Push(box);
        box.gameObject.transform.SetParent(boxParent.transform);
        box.gameObject.transform.localPosition = Vector3.zero;
    }
    public void pushTigh(Obstacle tigh)
    {
        tigh.gameObject.SetActive(false);
        tighWeWantToPool.pooledObjs.Push(tigh);
        tigh.gameObject.transform.SetParent(tighParent.transform);
        tigh.gameObject.transform.localPosition = Vector3.zero;
    }
  
    public Bullet GetBullet()
    {
            Bullet p = bulletsWeWantToPool.pooledObjs.Pop();
            p.gameObject.SetActive(true);
            p.gameObject.transform.SetParent(null);
            return p;   
    }
    public Bullet GetEBullet()
    {

        Bullet p = EnemyBulletWeWantToPool.pooledObjs.Pop();
        p.gameObject.SetActive(true);
        p.gameObject.transform.SetParent(null);
        return p;

    }
    public Tile GetTile()
    {

        Tile p = TileWeWantToPool.pooledObjs.Pop();
        p.gameObject.SetActive(true);
        p.gameObject.transform.SetParent(null);
        return p;

    }
    public move_enemy GetMove()
    {

        move_enemy p = moveWeWantToPool.pooledObjs.Pop();
        p.gameObject.SetActive(true);
        p.gameObject.transform.SetParent(null);
        return p;

    }
    public fade_enemy GetFade()
    {

        fade_enemy p = fadeEnemyWeWantToPool.pooledObjs.Pop();
        p.gameObject.SetActive(true);
        p.gameObject.transform.SetParent(null);
        return p;

    }
    public fire_enemy GetFire()
    {

        fire_enemy p = fireWeWantToPool.pooledObjs.Pop();
        p.gameObject.SetActive(true);
        p.gameObject.transform.SetParent(null);
        return p;

    }
    public Obstacle GetBox()
    {

        Obstacle p = boxWeWantToPool.pooledObjs.Pop();
        p.gameObject.SetActive(true);
        p.gameObject.transform.SetParent(null);
        return p;

    }
    public Obstacle GetTigh()
    {

        Obstacle p = tighWeWantToPool.pooledObjs.Pop();
        p.gameObject.SetActive(true);
        p.gameObject.transform.SetParent(null);
        return p;

    }

}

