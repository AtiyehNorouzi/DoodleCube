﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : MonoBehaviour {

    public enum gameState {before , pause , isStarted}
    public static gameState gamestate;
    private static GameState _instance;
    public static GameState instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<GameState>();
            return _instance;
        }
    }
    void Start()
    {
        InputController.instance._OnPressed += InputListener;
    }
    void InputListener(InputController.InputType type)
    {

        if (type == InputController.InputType.AnyKey && gamestate == gameState.before) 
            OnStarted();
    }
   void OnStarted()
    {
        gamestate = gameState.isStarted;
    }
    public void Pause()
    {
        //GameState.gamestate = gameState.pause;
        UiController.instance.ActiveRetryPanel();
     
    }

}
