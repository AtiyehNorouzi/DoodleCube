﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovable  {
    
    IEnumerator CheckDeadZone();
    IEnumerator Move(Vector3 dir , float speed);
    void Despawn();
}
